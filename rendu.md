# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Curoux, Jérémy, jeremy.curoux.etu@univ-lille.fr

- Nom, Prénom, email: Demade, Clément, clement.demade.etu@univ-lille.fr

## Question 1

le processus ne peut pas écrire car les priorités des droits ce font de gauche a droite ceux qui veut dire que si l'utilisateur n'a pas le droit d'écrire dans le fichier meme si il appartient a un groupe qui a les droits il ne pourras pas écrire dans le fichier.

## Question 2

-Le x pour un répertoire signifie le droit d'accéder a sont contenu

-On obtient permission denied car toto fait parti du groupe ubuntu qui n'a pas le droit de consulté le répertoire  

-On peut les fichier présent dans le repertoire cependant on ne voit pas les informations a par le nom du fichier car l'utilisateur a comme la permission read qui lui permet de regardé le contenu du répertoire   

## Question 3

les ids sont :
euid : 1001
egid : 1000
ruid : 1001
rgid : 1000
le fichier ne peut pas etre ouvrir 
Cannot open file: Permission denied

on obtient avec les flag

euid : 1000 
egid : 1000
ruid : 1001
rgid : 1000

le fichier s'ouvre
File opens correctly
## Question 4
avec suid.py ont obtient 

EUID :  1001
EGID :  1000

## Question 5

On obtient la ligne suivante :
-rwsr-xr-x 1 root root 85064 May 28 2020 /usr/bin/chfn

lorsque l'on lance la commande "ls -al /usr/bin/chfn"
pour les permission nous avons:
- "-" pour le fait qu'il ne sagit pas d'un répertoire
- "rws" = pour les permission du proprietaire, ils peuvents lire et écrire et le s signifie que l'utilisateur a temporairement les meme droit que le proprietaire du fichier.   
- "r-x" pour les permission du groupe, ils peuvents lire et executé
- "r-x" pour les autres utilisateurs, ils peuvents lire et executé

## Question 6

Les mots de passe sont stocké dans le fichier shadow qui sont présent dans le répertoire etc

Les mots de passe ne sont pas stocké au meme endroit car ce sont les hash qui y sont stocké

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.
