# Création de l'administrateur
adduser admin

# Creation des deux groupes a et b et c
addgroup groupe_a
addgroup groupe_b
addgroup groupe_c

# Ajout de l'administrateur dans les trois groupes
adduser admin groupe_a
adduser admin groupe_b
adduser admin groupe_c

# Création des utilisateur lambda_a et lambda_b ansi que de les ajouté a leur groupe respectif
adduser lambda_a --ingroup groupe_a
adduser lambda_b --ingroup groupe_b
adduser lambda_a groupe_c
adduser lambda_b groupe_c
